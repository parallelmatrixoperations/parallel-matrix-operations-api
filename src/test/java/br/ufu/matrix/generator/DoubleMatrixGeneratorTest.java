package br.ufu.matrix.generator;

import br.ufu.matrix.generator.range.Range;
import br.ufu.matrix.utils.MatrixUtils;
import org.junit.jupiter.api.Test;

class DoubleMatrixGeneratorTest {

    @Test
    void testGenerator() {
        DoubleMatrixGenerator doubleMatrixGenerator = new DoubleMatrixGenerator(
                5, 5, new Range(0, 10)
        );

        Double[][] matrix = doubleMatrixGenerator.build();

        MatrixUtils.prettyPrint(matrix);
    }
}
