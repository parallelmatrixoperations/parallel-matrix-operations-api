package br.ufu.matrix.task.division;

import br.ufu.matrix.generator.DoubleMatrixGenerator;
import br.ufu.matrix.generator.range.Range;
import br.ufu.matrix.task.async.division.AsyncMatrixDivision;
import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;
import br.ufu.matrix.task.sync.division.SyncMatrixDivision;
import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;
import br.ufu.matrix.utils.MatrixUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class DivisionTest {

    @Test
    void asyncCalcAssertion() throws SyncOperationException {
        DoubleMatrixGenerator matrixGeneratorOne = new DoubleMatrixGenerator()
                .columns(4)
                .lines(4)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(100)
                );

        DoubleMatrixGenerator matrixGeneratorTwo = new DoubleMatrixGenerator()
                .columns(4)
                .lines(4)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(100)
                );

        Double[][] matrixOne = matrixGeneratorOne.build();
        Double[][] matrixTwo = matrixGeneratorTwo.build();

        MatrixUtils.prettyPrint(matrixOne);
        System.out.println();
        MatrixUtils.prettyPrint(matrixTwo);
        System.out.println();

        Double[][] matrixResult = new Double[4][4];

        AsyncOperationCallback<Double[]> callback = new AsyncOperationCallback<Double[]>() {
            @Override
            public void success(Double[] result, Integer line, Integer column) {
                matrixResult[line] = result;
            }

            @Override
            public void error(AsyncOperationException error, Integer line, Integer column) {
                error.printStackTrace();
            }
        };

        for (int i = 0; i < 4; i++)
            new AsyncMatrixDivision(callback, matrixOne, matrixTwo, i).run();

        MatrixUtils.prettyPrint(matrixResult);
        System.out.println();

        Double[][] matrixSyncResult = new SyncMatrixDivision(matrixOne, matrixTwo)
                .execute();

        MatrixUtils.prettyPrint(matrixSyncResult);

        Assertions.assertArrayEquals(matrixResult, matrixSyncResult);
        Assertions.assertTrue(Arrays.deepEquals(matrixResult, matrixSyncResult));
    }
}
