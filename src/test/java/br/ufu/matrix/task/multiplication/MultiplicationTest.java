package br.ufu.matrix.task.multiplication;

import br.ufu.matrix.generator.DoubleMatrixGenerator;
import br.ufu.matrix.generator.range.Range;
import br.ufu.matrix.task.async.multiplication.AsyncMatrixMultiplication;
import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;
import br.ufu.matrix.task.sync.multiplication.SyncMatrixMultiplication;
import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;
import br.ufu.matrix.utils.MatrixUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class MultiplicationTest {

    @Test
    void asyncCalcAssertion() throws SyncOperationException {
        DoubleMatrixGenerator matrixGeneratorOne = new DoubleMatrixGenerator()
                .columns(3)
                .lines(2)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(100)
                );

        DoubleMatrixGenerator matrixGeneratorTwo = new DoubleMatrixGenerator()
                .columns(2)
                .lines(3)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(100)
                );

        Double[][] matrixOne = matrixGeneratorOne.build();
        Double[][] matrixTwo = matrixGeneratorTwo.build();

        MatrixUtils.prettyPrint(matrixOne);
        System.out.println();
        MatrixUtils.prettyPrint(matrixTwo);
        System.out.println();

        Double[][] matrixResult = new Double[2][2];

        AsyncOperationCallback<Double> callback = new AsyncOperationCallback<Double>() {
            @Override
            public void success(Double result, Integer line, Integer column) {
                matrixResult[line][column] = result;
            }

            @Override
            public void error(AsyncOperationException error, Integer line, Integer column) {
                error.printStackTrace();
            }
        };

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                new AsyncMatrixMultiplication(callback, matrixOne, matrixTwo, i, j).run();

        MatrixUtils.prettyPrint(matrixResult);
        System.out.println();

        Double[][] matrixSyncResult = new SyncMatrixMultiplication(matrixOne, matrixTwo)
                .execute();

        MatrixUtils.prettyPrint(matrixSyncResult);

        Assertions.assertArrayEquals(matrixResult, matrixSyncResult);
        Assertions.assertTrue(Arrays.deepEquals(matrixResult, matrixSyncResult));
    }
}
