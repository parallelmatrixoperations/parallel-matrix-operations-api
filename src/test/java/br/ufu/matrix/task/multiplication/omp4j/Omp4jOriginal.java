package br.ufu.matrix.task.multiplication.omp4j;

import br.ufu.matrix.task.sync.operation.SyncOperation;

import static java.util.Objects.isNull;

public class Omp4jOriginal extends SyncOperation<Double> {

    public Omp4jOriginal(Double[][] matrixOne, Double[][] matrixTwo) {
        super(matrixOne, matrixTwo);
    }

    @Override
    public Double[][] execute() {
        Double[][] matrixOne = getMatrixOne();
        Double[][] matrixTwo = getMatrixTwo();

        int matrixOneLinesCount = matrixOne.length;

        Double[] firstMatrixOneLine = matrixOne[0];
        Double[] firstMatrixTwoLine = matrixTwo[0];

        int matrixOneColumnsCount = firstMatrixOneLine.length;
        int matrixTwoColumnsCount = firstMatrixTwoLine.length;

        Double[][] result = new Double[matrixOneLinesCount][matrixTwoColumnsCount];

        // omp parallel for
        for (int i = 0; i < matrixOneLinesCount; i++) {
            Double[] matrixOneLine = matrixOne[i];

            for (int j = 0; j < matrixTwoColumnsCount; j++)
                for (int k = 0; k < matrixOneColumnsCount; k++) {
                    Double[] matrixTwoLine = matrixTwo[k];

                    Double matrixOneColumnValue = matrixOneLine[k];
                    Double matrixTwoColumnValue = matrixTwoLine[j];

                    Double multiplicationResult = matrixTwoColumnValue * matrixOneColumnValue;

                    if (isNull(result[i][j]))
                        result[i][j] = multiplicationResult;
                    else
                        result[i][j] += multiplicationResult;
                }
        }

        return result;
    }
}
