package br.ufu.matrix.task.multiplication.performance;

import br.ufu.matrix.generator.DoubleMatrixGenerator;
import br.ufu.matrix.generator.range.Range;
import br.ufu.matrix.task.async.executor.AsyncOperationExecutor;
import br.ufu.matrix.task.async.executor.callback.AsyncOperationExecutorCallback;
import br.ufu.matrix.task.async.executor.exception.AsyncOperationExecutorException;
import br.ufu.matrix.task.async.multiplication.AsyncMatrixMultiplicationExecutor;
import br.ufu.matrix.task.multiplication.omp4j.Omp4jConverted;
import br.ufu.matrix.task.sync.multiplication.SyncMatrixMultiplication;
import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;
import org.junit.jupiter.api.Test;
import org.omp4j.runtime.DynamicExecutor;
import org.omp4j.runtime.IOMPExecutor;

import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

class MultiplicationExecutorPerformanceTest {

    // 16 VS 17 (1 segundo de ganho)

    private static final int[] THREADS = {1, 5, 10, 20};

    @Test
    void async() throws AsyncOperationExecutorException, InterruptedException {
        Semaphore semaphore = new Semaphore(1);

        DoubleMatrixGenerator matrixGenerator = new DoubleMatrixGenerator()
                .columns(1000)
                .lines(1000)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(5)
                );

        Double[][] matrixOne = matrixGenerator.build();
        Double[][] matrixTwo = matrixGenerator.build();

        AsyncOperationExecutor<Double, Double> executor = new AsyncMatrixMultiplicationExecutor()
                .matrixOne(matrixOne)
                .matrixTwo(matrixTwo)
                .build();

        AsyncOperationExecutorCallback<Double> callback = new AsyncOperationExecutorCallback<Double>() {
            @Override
            public void success(Double[][] result) {
                semaphore.release();
            }

            @Override
            public void error(AsyncOperationExecutorException error) {
                error.printStackTrace();
            }
        };

        for (int j = 0; j < 4; j++) {
            long avg = 0;

            int threads = THREADS[j];

            for (int i = 0; i < 100; i++) {
                semaphore.acquire();

                long start = new Date().getTime();

                executor.execute(callback);

                semaphore.acquire();

                long end = new Date().getTime();

                avg += end - start;

                semaphore.release();

                executor.threads(threads);

                executor = executor.build();
            }

            avg /= 100;

            System.out.println("Async: " + TimeUnit.MILLISECONDS.toSeconds(avg) + "s");
        }
    }

    @Test
    void sync() throws SyncOperationException {
        DoubleMatrixGenerator matrixGenerator = new DoubleMatrixGenerator()
                .columns(1000)
                .lines(1000)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(5)
                );

        Double[][] matrixOne = matrixGenerator.build();
        Double[][] matrixTwo = matrixGenerator.build();

        long avg = 0;

        for (int i = 0; i < 100; i++) {
            long start = new Date().getTime();

            new SyncMatrixMultiplication(matrixOne, matrixTwo)
                    .execute();

            long end = new Date().getTime();

            avg += end - start;
        }

        avg /= 100;

        System.out.println("Sync: " + TimeUnit.MILLISECONDS.toSeconds(avg) + "s");
    }

    @Test
    void omp4j() {
        DoubleMatrixGenerator matrixGenerator = new DoubleMatrixGenerator()
                .columns(1000)
                .lines(1000)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(5)
                );

        Double[][] matrixOne = matrixGenerator.build();
        Double[][] matrixTwo = matrixGenerator.build();

        for (int j = 0; j < 4; j++) {
            long avg = 0;

            int threads = THREADS[j];

            for (int n = 0; n < 100; n++) {
                long start = new Date().getTime();

                new Omp4jConverted(matrixOne, matrixTwo, threads)
                        .execute();

                long end = new Date().getTime();

                avg += end - start;
            }

            avg /= 100;

            System.out.println("Omp4j: " + TimeUnit.MILLISECONDS.toSeconds(avg) + "s");
        }
    }
}
