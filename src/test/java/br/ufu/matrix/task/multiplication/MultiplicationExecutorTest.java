package br.ufu.matrix.task.multiplication;

import br.ufu.matrix.generator.DoubleMatrixGenerator;
import br.ufu.matrix.generator.range.Range;
import br.ufu.matrix.task.async.executor.callback.AsyncOperationExecutorCallback;
import br.ufu.matrix.task.async.executor.exception.AsyncOperationExecutorException;
import br.ufu.matrix.task.async.multiplication.AsyncMatrixMultiplicationExecutor;
import br.ufu.matrix.task.sync.multiplication.SyncMatrixMultiplication;
import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class MultiplicationExecutorTest {

    @Test
    void async() throws AsyncOperationExecutorException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        DoubleMatrixGenerator matrixGenerator = new DoubleMatrixGenerator()
                .columns(1000)
                .lines(1000)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(5)
                );

        AsyncOperationExecutorCallback<Double> callback = new AsyncOperationExecutorCallback<Double>() {
            @Override
            public void success(Double[][] result) {
                countDownLatch.countDown();
            }

            @Override
            public void error(AsyncOperationExecutorException error) {
                error.printStackTrace();
            }
        };

        Double[][] matrixOne = matrixGenerator.build();
        Double[][] matrixTwo = matrixGenerator.build();

        long start = new Date().getTime();

        new AsyncMatrixMultiplicationExecutor()
                .matrixOne(matrixOne)
                .matrixTwo(matrixTwo)
                .build()
                .execute(callback);

        System.out.println("Soltei");

        countDownLatch.await();

        long end = new Date().getTime();

        System.out.println("Async: " + TimeUnit.MILLISECONDS.toSeconds(end - start) + "s");
    }

    @Test
    void sync() throws SyncOperationException {
        DoubleMatrixGenerator matrixGenerator = new DoubleMatrixGenerator()
                .columns(1000)
                .lines(1000)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(5)
                );

        Double[][] matrixOne = matrixGenerator.build();
        Double[][] matrixTwo = matrixGenerator.build();

        long start = new Date().getTime();

        new SyncMatrixMultiplication(matrixOne, matrixTwo)
                .execute();

        long end = new Date().getTime();

        System.out.println("Sync: " + TimeUnit.MILLISECONDS.toSeconds(end - start) + "s");
    }

    @Test
    void assertion() throws AsyncOperationExecutorException, InterruptedException, SyncOperationException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        DoubleMatrixGenerator matrixGenerator = new DoubleMatrixGenerator()
                .columns(1000)
                .lines(1000)
                .range(
                        new Range()
                                .minValue(1)
                                .maxValue(100)
                );

        Double[][] matrixOne = matrixGenerator.build();
        Double[][] matrixTwo = matrixGenerator.build();

        Double[][] resultSync = new SyncMatrixMultiplication(matrixOne, matrixTwo)
                .execute();

        System.out.println("Sync ended");

        AsyncOperationExecutorCallback<Double> callback = new AsyncOperationExecutorCallback<Double>() {
            @Override
            public void success(Double[][] result) {
                System.out.println("Async ended");

                Assertions.assertArrayEquals(result, resultSync);
                Assertions.assertTrue(Arrays.deepEquals(result, resultSync));

                countDownLatch.countDown();
            }

            @Override
            public void error(AsyncOperationExecutorException error) {
                error.printStackTrace();
            }
        };

        new AsyncMatrixMultiplicationExecutor()
                .matrixOne(matrixOne)
                .matrixTwo(matrixTwo)
                .build()
                .execute(callback);

        countDownLatch.await();
    }
}
