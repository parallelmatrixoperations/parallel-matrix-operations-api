package br.ufu.matrix.task.multiplication.omp4j;

import br.ufu.matrix.task.sync.operation.SyncOperation;
import org.omp4j.runtime.DynamicExecutor;
import org.omp4j.runtime.IOMPExecutor;

import static java.util.Objects.isNull;

public class Omp4jConverted extends SyncOperation<Double> {

    private Integer threadCount;

    public Omp4jConverted(Double[][] matrixOne, Double[][] matrixTwo, Integer threadCount) {
        super(matrixOne, matrixTwo);
        this.threadCount = threadCount;
    }

    @Override
    public Double[][] execute() {
        Double[][] matrixOne = getMatrixOne();
        Double[][] matrixTwo = getMatrixTwo();

        int matrixOneLinesCount = matrixOne.length;

        Double[] firstMatrixOneLine = matrixOne[0];
        Double[] firstMatrixTwoLine = matrixTwo[0];

        int matrixOneColumnsCount = firstMatrixOneLine.length;
        int matrixTwoColumnsCount = firstMatrixTwoLine.length;

        Double[][] result = new Double[matrixOneLinesCount][matrixTwoColumnsCount];

        /* === OMP CONTEXT === */
        class OMPContext {
            private Double[][] local_result;
            private Double[][] local_matrixTwo;
            private int local_matrixOneColumnsCount;
            private Double[][] local_matrixOne;
            private int local_matrixOneLinesCount;
            private int local_matrixTwoColumnsCount;
        }

        final OMPContext ompContext = new OMPContext();
        ompContext.local_matrixOne = matrixOne;
        ompContext.local_matrixOneColumnsCount = matrixOneColumnsCount;
        ompContext.local_matrixTwo = matrixTwo;
        ompContext.local_matrixOneLinesCount = matrixOneLinesCount;
        ompContext.local_result = result;
        ompContext.local_matrixTwoColumnsCount = matrixTwoColumnsCount;

        final IOMPExecutor ompExecutor = new DynamicExecutor(threadCount);
        /* === /OMP CONTEXT === */

        for (int i_IjR = 0; i_IjR < ompContext.local_matrixOneLinesCount; i_IjR++) {
            final int i = i_IjR;
            ompExecutor.execute(() -> {
                Double[] matrixOneLine = ompContext.local_matrixOne[i];

                for (int j = 0; j < ompContext.local_matrixTwoColumnsCount; j++)
                    for (int k = 0; k < ompContext.local_matrixOneColumnsCount; k++) {
                        Double[] matrixTwoLine = ompContext.local_matrixTwo[k];

                        Double matrixOneColumnValue = matrixOneLine[k];
                        Double matrixTwoColumnValue = matrixTwoLine[j];

                        Double multiplicationResult = matrixTwoColumnValue * matrixOneColumnValue;

                        if (isNull(ompContext.local_result[i][j]))
                            ompContext.local_result[i][j] = multiplicationResult;
                        else
                            ompContext.local_result[i][j] += multiplicationResult;
                    }
            });
        }
        ompExecutor.waitForExecution();


        return result;
    }
}
