package br.ufu.matrix.generator.range;

import br.ufu.matrix.generator.builder.Builder;

import java.util.concurrent.ThreadLocalRandom;

import static java.util.Objects.isNull;

public class Range implements Builder<Double> {

    private int minValue;

    private int maxValue;

    public Range() {
    }

    public Range(int minValue, int maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public Range minValue(int minValue) {
        this.minValue = minValue;

        return this;
    }

    public Range maxValue(int maxValue) {
        this.maxValue = maxValue;

        return this;
    }

    @Override
    public Double build() {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        if (!isNull(minValue) && !isNull(maxValue))
            return random.nextDouble(minValue, maxValue);

        return random.nextDouble();
    }
}
