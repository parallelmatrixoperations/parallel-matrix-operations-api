package br.ufu.matrix.generator.builder;

public interface Builder<T> {

    T build();
}
