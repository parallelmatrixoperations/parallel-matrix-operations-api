package br.ufu.matrix.generator;

import br.ufu.matrix.generator.builder.Builder;
import br.ufu.matrix.generator.range.Range;

import static java.util.Objects.isNull;

public class DoubleMatrixGenerator implements Builder<Double[][]> {

    private int lines = 100;

    private int columns = 100;

    private Range range;

    public DoubleMatrixGenerator() {
    }

    public DoubleMatrixGenerator(int lines, int columns, Range range) {
        this.lines = lines;
        this.columns = columns;
        this.range = range;
    }

    public DoubleMatrixGenerator lines(int lines) {
        this.lines = lines;

        return this;
    }

    public DoubleMatrixGenerator columns(int columns) {
        this.columns = columns;

        return this;
    }

    public DoubleMatrixGenerator range(Range range) {
        this.range = range;

        return this;
    }

    @Override
    public Double[][] build() {
        Double[][] result = new Double[lines][columns];

        Range randomDoubleGenerator = !isNull(range) ?
                range :
                new Range();

        for (int i = 0; i < lines; i++)
            for (int j = 0; j < columns; j++)
                result[i][j] = randomDoubleGenerator.build();

        return result;
    }
}
