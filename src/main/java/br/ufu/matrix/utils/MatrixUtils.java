package br.ufu.matrix.utils;

import static java.util.Objects.isNull;

public class MatrixUtils {

    public static void prettyPrint(Number[][] matrix) {
        if (isNull(matrix))
            return;

        int lines = matrix.length;

        if (lines == 0)
            return;

        int columns = matrix[0].length;

        if (columns == 0)
            return;

        String result = "|\t";

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < columns; j++)
                result += matrix[i][j] + "\t";

            System.out.println(result + "|");
            result = "|\t";
        }
    }
}
