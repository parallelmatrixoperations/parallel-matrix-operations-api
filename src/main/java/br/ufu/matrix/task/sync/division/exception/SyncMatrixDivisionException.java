package br.ufu.matrix.task.sync.division.exception;

import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;

public class SyncMatrixDivisionException extends SyncOperationException {

    private static final long serialVersionUID = 4844338112978533930L;

    public SyncMatrixDivisionException() {
    }

    public SyncMatrixDivisionException(String message) {
        super(message);
    }

    public SyncMatrixDivisionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyncMatrixDivisionException(Throwable cause) {
        super(cause);
    }

    public SyncMatrixDivisionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
