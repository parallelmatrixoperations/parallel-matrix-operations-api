package br.ufu.matrix.task.sync.multiplication;

import br.ufu.matrix.task.sync.multiplication.exception.SyncMatrixMultiplicationException;
import br.ufu.matrix.task.sync.operation.SyncOperation;
import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;

import static java.util.Objects.isNull;

public class SyncMatrixMultiplication extends SyncOperation<Double> {

    public SyncMatrixMultiplication(Double[][] matrixOne, Double[][] matrixTwo) {
        super(matrixOne, matrixTwo);
    }

    @Override
    public Double[][] execute() throws SyncOperationException {
        try {
            Double[][] matrixOne = getMatrixOne();
            Double[][] matrixTwo = getMatrixTwo();

            if (isNull(matrixOne))
                throw new SyncMatrixMultiplicationException("Matrix one is null");

            if (isNull(matrixTwo))
                throw new SyncMatrixMultiplicationException("Matrix two is null");

            int matrixOneLinesCount = matrixOne.length;
            int matrixTwoLinesCount = matrixTwo.length;

            Double[] firstMatrixOneLine = matrixOne[0];
            Double[] firstMatrixTwoLine = matrixTwo[0];

            if (isNull(firstMatrixOneLine))
                throw new SyncMatrixMultiplicationException("Matrix one null line");

            if (isNull(firstMatrixTwoLine))
                throw new SyncMatrixMultiplicationException("Matrix two null line");

            int matrixOneColumnsCount = firstMatrixOneLine.length;
            int matrixTwoColumnsCount = firstMatrixTwoLine.length;

            if (matrixOneColumnsCount != matrixTwoLinesCount)
                throw new SyncMatrixMultiplicationException("Invalid multiplication");

            Double[][] result = new Double[matrixOneLinesCount][matrixTwoColumnsCount];

            for (int i = 0; i < matrixOneLinesCount; i++) {
                Double[] matrixOneLine = matrixOne[i];

                if (isNull(matrixOneLine))
                    throw new SyncMatrixMultiplicationException("Matrix one null line");

                for (int j = 0; j < matrixTwoColumnsCount; j++)
                    for (int k = 0; k < matrixOneColumnsCount; k++) {
                        Double[] matrixTwoLine = matrixTwo[k];

                        if (isNull(matrixTwoLine))
                            throw new SyncMatrixMultiplicationException("Matrix two null line");

                        Double matrixOneColumnValue = matrixOneLine[k];
                        Double matrixTwoColumnValue = matrixTwoLine[j];

                        if (isNull(matrixOneColumnValue))
                            throw new SyncMatrixMultiplicationException("Matrix one null column value");

                        if (isNull(matrixTwoColumnValue))
                            throw new SyncMatrixMultiplicationException("Matrix two null column value");

                        Double multiplicationResult = matrixTwoColumnValue * matrixOneColumnValue;

                        if (isNull(result[i][j]))
                            result[i][j] = multiplicationResult;
                        else
                            result[i][j] += multiplicationResult;
                    }
            }

            return result;
        } catch (SyncMatrixMultiplicationException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new SyncMatrixMultiplicationException(exception);
        }
    }
}
