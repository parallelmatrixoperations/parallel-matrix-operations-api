package br.ufu.matrix.task.sync.division;

import br.ufu.matrix.task.sync.division.exception.SyncMatrixDivisionException;
import br.ufu.matrix.task.sync.operation.SyncOperation;
import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;

import static java.util.Objects.isNull;

public class SyncMatrixDivision extends SyncOperation<Double> {

    public SyncMatrixDivision(Double[][] matrixOne, Double[][] matrixTwo) {
        super(matrixOne, matrixTwo);
    }

    @Override
    public Double[][] execute() throws SyncOperationException {
        try {
            Double[][] matrixOne = getMatrixOne();
            Double[][] matrixTwo = getMatrixTwo();

            if (isNull(matrixOne))
                throw new SyncMatrixDivisionException("Matrix one is null");

            if (isNull(matrixTwo))
                throw new SyncMatrixDivisionException("Matrix two is null");

            int matrixOneLinesCount = matrixOne.length;
            int matrixTwoLinesCount = matrixTwo.length;

            Double[] firstMatrixOneLine = matrixOne[0];
            Double[] firstMatrixTwoLine = matrixTwo[0];

            if (isNull(firstMatrixOneLine))
                throw new SyncMatrixDivisionException("Matrix one null line");

            if (isNull(firstMatrixTwoLine))
                throw new SyncMatrixDivisionException("Matrix two null line");

            int matrixOneColumnsCount = firstMatrixOneLine.length;
            int matrixTwoColumnsCount = firstMatrixTwoLine.length;

            if (matrixOneColumnsCount != matrixTwoColumnsCount)
                throw new SyncMatrixDivisionException("Invalid division");

            Double[][] result = new Double[matrixOneLinesCount][matrixOneColumnsCount];

            for (int i = 0; i < matrixOneLinesCount; i++) {
                Double[] matrixOneLine = matrixOne[i];
                Double[] matrixTwoLine = matrixTwo[i];

                if (isNull(matrixOneLine))
                    throw new SyncMatrixDivisionException("Matrix one null line");

                if (isNull(matrixTwoLine))
                    throw new SyncMatrixDivisionException("Matrix two null line");

                for (int j = 0; j < matrixOneColumnsCount; j++) {
                    Double matrixOneColumnValue = matrixOneLine[j];
                    Double matrixTwoColumnValue = matrixTwoLine[j];

                    if (isNull(matrixOneColumnValue))
                        throw new SyncMatrixDivisionException("Matrix one null column value");

                    if (isNull(matrixTwoColumnValue))
                        throw new SyncMatrixDivisionException("Matrix two null column value");

                    result[i][j] = matrixOneColumnValue / matrixTwoColumnValue;
                }
            }

            return result;
        } catch (SyncMatrixDivisionException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new SyncMatrixDivisionException(exception);
        }
    }
}
