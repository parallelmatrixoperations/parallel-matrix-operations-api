package br.ufu.matrix.task.sync.operation.exception;

public class SyncOperationException extends Exception {

    private static final long serialVersionUID = -5478684147822254493L;

    public SyncOperationException() {
    }

    public SyncOperationException(String message) {
        super(message);
    }

    public SyncOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyncOperationException(Throwable cause) {
        super(cause);
    }

    public SyncOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
