package br.ufu.matrix.task.sync.multiplication.exception;

import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;

public class SyncMatrixMultiplicationException extends SyncOperationException {

    private static final long serialVersionUID = 2467812564728618317L;

    public SyncMatrixMultiplicationException() {
    }

    public SyncMatrixMultiplicationException(String message) {
        super(message);
    }

    public SyncMatrixMultiplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyncMatrixMultiplicationException(Throwable cause) {
        super(cause);
    }

    public SyncMatrixMultiplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
