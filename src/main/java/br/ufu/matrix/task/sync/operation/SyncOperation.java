package br.ufu.matrix.task.sync.operation;

import br.ufu.matrix.task.sync.operation.exception.SyncOperationException;

public abstract class SyncOperation<T> {

    private final T[][] matrixOne;
    private final T[][] matrixTwo;

    public SyncOperation(T[][] matrixOne, T[][] matrixTwo) {
        this.matrixOne = matrixOne;
        this.matrixTwo = matrixTwo;
    }

    public abstract T[][] execute() throws SyncOperationException;

    public T[][] getMatrixOne() {
        return matrixOne;
    }

    public T[][] getMatrixTwo() {
        return matrixTwo;
    }
}
