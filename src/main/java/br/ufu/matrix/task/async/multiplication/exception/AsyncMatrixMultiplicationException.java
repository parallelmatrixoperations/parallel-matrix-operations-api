package br.ufu.matrix.task.async.multiplication.exception;

import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

public class AsyncMatrixMultiplicationException extends AsyncOperationException {

    private static final long serialVersionUID = -7872873009054552691L;

    public AsyncMatrixMultiplicationException() {
    }

    public AsyncMatrixMultiplicationException(String message) {
        super(message);
    }

    public AsyncMatrixMultiplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AsyncMatrixMultiplicationException(Throwable cause) {
        super(cause);
    }

    public AsyncMatrixMultiplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
