package br.ufu.matrix.task.async.multiplication;

import br.ufu.matrix.task.async.multiplication.exception.AsyncMatrixMultiplicationException;
import br.ufu.matrix.task.async.operation.AsyncOperation;
import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

import static java.util.Objects.isNull;

public class AsyncMatrixMultiplication extends AsyncOperation<Double, Double> {

    public AsyncMatrixMultiplication(AsyncOperationCallback<Double> callback,
                                     Double[][] matrixOne, Double[][] matrixTwo,
                                     Integer lineIndex, Integer columnIndex) {
        super(callback, matrixOne, matrixTwo, lineIndex, columnIndex);
    }

    public Double execute() throws AsyncOperationException {
        try {
            Double[][] matrixOne = getMatrixOne();
            Double[][] matrixTwo = getMatrixTwo();

            if (isNull(matrixOne))
                throw new AsyncMatrixMultiplicationException("Matrix one is null");

            if (isNull(matrixTwo))
                throw new AsyncMatrixMultiplicationException("Matrix two is null");

            int matrixTwoLinesCount = matrixTwo.length;

            int lineIndex = getLineIndex();
            int columnIndex = getColumnIndex();

            if (isNull(lineIndex))
                throw new AsyncMatrixMultiplicationException("Line index is null");

            if (isNull(columnIndex))
                throw new AsyncMatrixMultiplicationException("Column index is null");

            Double[] matrixOneLine = matrixOne[lineIndex];

            if (isNull(matrixOneLine))
                throw new AsyncMatrixMultiplicationException("Matrix one null line");

            int matrixOneColumnsCount = matrixOneLine.length;

            if (matrixOneColumnsCount != matrixTwoLinesCount)
                throw new AsyncMatrixMultiplicationException("Invalid multiplication");

            double result = 0D;

            for (int i = 0; i < matrixOneColumnsCount; i++) {
                Double[] matrixTwoLine = matrixTwo[i];

                if (isNull(matrixTwoLine))
                    throw new AsyncMatrixMultiplicationException("Matrix two null line");

                Double matrixOneColumnValue = matrixOneLine[i];
                Double matrixTwoColumnValue = matrixTwoLine[columnIndex];

                if (isNull(matrixOneColumnValue))
                    throw new AsyncMatrixMultiplicationException("Matrix one null column value");

                if (isNull(matrixTwoColumnValue))
                    throw new AsyncMatrixMultiplicationException("Matrix two null column value");

                result += matrixTwoColumnValue * matrixOneColumnValue;
            }

            return result;
        } catch (AsyncMatrixMultiplicationException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new AsyncMatrixMultiplicationException(exception);
        }
    }
}
