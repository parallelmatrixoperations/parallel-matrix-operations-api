package br.ufu.matrix.task.async.operation.callback;

import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

public interface AsyncOperationCallback<T> {

    void success(T result, Integer line, Integer column);

    void error(AsyncOperationException error, Integer line, Integer column);
}
