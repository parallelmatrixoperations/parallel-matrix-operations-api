package br.ufu.matrix.task.async.operation;

import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

public abstract class AsyncOperation<T, E> implements Runnable {

    private final AsyncOperationCallback<T> callback;

    private final E[][] matrixOne;
    private final E[][] matrixTwo;

    private final Integer lineIndex;

    private final Integer columnIndex;

    public AsyncOperation(AsyncOperationCallback<T> callback, E[][] matrixOne, E[][] matrixTwo, Integer lineIndex, Integer columnIndex) {
        this.callback = callback;
        this.matrixOne = matrixOne;
        this.matrixTwo = matrixTwo;
        this.lineIndex = lineIndex;
        this.columnIndex = columnIndex;
    }

    public abstract T execute() throws AsyncOperationException;

    @Override
    public void run() {
        try {
            callback.success(execute(), getLineIndex(), getColumnIndex());
        } catch (AsyncOperationException exception) {
            callback.error(exception, getLineIndex(), getColumnIndex());
        }
    }

    public AsyncOperationCallback<T> getCallback() {
        return callback;
    }

    public E[][] getMatrixOne() {
        return matrixOne;
    }

    public E[][] getMatrixTwo() {
        return matrixTwo;
    }

    public Integer getLineIndex() {
        return lineIndex;
    }

    public Integer getColumnIndex() {
        return columnIndex;
    }
}
