package br.ufu.matrix.task.async.executor.exception;

public class AsyncOperationExecutorException extends Exception {

    private static final long serialVersionUID = -8410780690865556056L;

    public AsyncOperationExecutorException() {
    }

    public AsyncOperationExecutorException(String message) {
        super(message);
    }

    public AsyncOperationExecutorException(String message, Throwable cause) {
        super(message, cause);
    }

    public AsyncOperationExecutorException(Throwable cause) {
        super(cause);
    }

    public AsyncOperationExecutorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
