package br.ufu.matrix.task.async.division.exception;

import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

public class AsyncMatrixDivisionException extends AsyncOperationException {

    private static final long serialVersionUID = 488180847478944771L;

    public AsyncMatrixDivisionException() {
    }

    public AsyncMatrixDivisionException(String message) {
        super(message);
    }

    public AsyncMatrixDivisionException(String message, Throwable cause) {
        super(message, cause);
    }

    public AsyncMatrixDivisionException(Throwable cause) {
        super(cause);
    }

    public AsyncMatrixDivisionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
