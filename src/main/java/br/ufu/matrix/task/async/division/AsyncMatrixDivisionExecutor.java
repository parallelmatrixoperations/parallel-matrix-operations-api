package br.ufu.matrix.task.async.division;

import br.ufu.matrix.task.async.executor.AsyncOperationExecutor;
import br.ufu.matrix.task.async.executor.callback.AsyncOperationExecutorCallback;
import br.ufu.matrix.task.async.executor.exception.AsyncOperationExecutorException;
import br.ufu.matrix.task.async.operation.AsyncOperation;
import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

import java.util.concurrent.RejectedExecutionException;

public class AsyncMatrixDivisionExecutor extends AsyncOperationExecutor<Double[], Double> {

    public AsyncMatrixDivisionExecutor() {
        super(Double.class);
    }

    @Override
    public void execute(AsyncOperationExecutorCallback<Double> callback) {
        AsyncOperationCallback<Double[]> operationCallback = new AsyncOperationCallback<Double[]>() {
            @Override
            public void success(Double[] result, Integer line, Integer column) {
                getResult()[line] = result;
                getCounter().decrementAndGet();

                if (getCounter().get() == 0)
                    callback.success(getResult());
            }

            @Override
            public void error(AsyncOperationException error, Integer line, Integer column) {
                callback.error(new AsyncOperationExecutorException("Error on division execution", error));
            }
        };

        try {
            getExecutorService()
                    .submit(new LineThread(operationCallback));
        } catch (RejectedExecutionException exception) {
            operationCallback.error(new AsyncOperationException(exception), null, null);
        }
    }

    @Override
    public int getCounterTotal() {
        return this.matrixOneLinesCount;
    }

    private class LineThread implements Runnable {

        private final AsyncOperationCallback<Double[]> operationCallback;

        private LineThread(AsyncOperationCallback<Double[]> operationCallback) {
            this.operationCallback = operationCallback;
        }

        @Override
        public void run() {
            int i = 0;

            try {
                for (i = 0; i < getMatrixOneLinesCount(); i++)
                    getExecutorService()
                            .submit(
                                    new AsyncMatrixDivision(
                                            this.operationCallback, getMatrixOne(), getMatrixTwo(), i
                                    )
                            );
            } catch (RejectedExecutionException exception) {
                operationCallback.error(new AsyncOperationException(exception), i, null);
            }
        }
    }
}
