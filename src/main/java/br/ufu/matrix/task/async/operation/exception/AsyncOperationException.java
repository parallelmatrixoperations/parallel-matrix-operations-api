package br.ufu.matrix.task.async.operation.exception;

public class AsyncOperationException extends Exception {

    private static final long serialVersionUID = -4348992286699880379L;

    public AsyncOperationException() {
    }

    public AsyncOperationException(String message) {
        super(message);
    }

    public AsyncOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AsyncOperationException(Throwable cause) {
        super(cause);
    }

    public AsyncOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
