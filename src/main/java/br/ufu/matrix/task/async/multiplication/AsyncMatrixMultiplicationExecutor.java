package br.ufu.matrix.task.async.multiplication;

import br.ufu.matrix.task.async.executor.AsyncOperationExecutor;
import br.ufu.matrix.task.async.executor.callback.AsyncOperationExecutorCallback;
import br.ufu.matrix.task.async.executor.exception.AsyncOperationExecutorException;
import br.ufu.matrix.task.async.operation.AsyncOperation;
import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

import java.util.concurrent.RejectedExecutionException;

public class AsyncMatrixMultiplicationExecutor extends AsyncOperationExecutor<Double, Double> {

    private final FinalCallback operationCallback = new FinalCallback();

    public AsyncMatrixMultiplicationExecutor() {
        super(Double.class);
    }

    @Override
    public void execute(AsyncOperationExecutorCallback<Double> callback) {
        operationCallback.setCallback(callback);

        try {
            getExecutorService()
                    .submit(new ExecutorThread(operationCallback));
        } catch (RejectedExecutionException exception) {
            operationCallback.error(new AsyncOperationException(exception), null, null);
        }
    }

    @Override
    public int getCounterTotal() {
        return this.matrixOneLinesCount * this.matrixTwoColumnsCount;
    }

    private class FinalCallback implements AsyncOperationCallback<Double> {

        private AsyncOperationExecutorCallback<Double> callback;

        private FinalCallback() {
        }

        @Override
        public void success(Double result, Integer line, Integer column) {
            getResult()[line][column] = result;
            getCounter().decrementAndGet();

            if (getCounter().get() == 0)
                callback.success(getResult());
        }

        @Override
        public void error(AsyncOperationException error, Integer line, Integer column) {
            callback.error(
                    new AsyncOperationExecutorException(
                            String.format(
                                    "Error on multiplication execution. Line %s, column %s",
                                    line, column
                            ),
                            error
                    )
            );
        }

        public AsyncOperationExecutorCallback<Double> getCallback() {
            return callback;
        }

        public void setCallback(AsyncOperationExecutorCallback<Double> callback) {
            this.callback = callback;
        }
    }

    private class ExecutorThread implements Runnable {

        private final AsyncOperationCallback<Double> operationCallback;

        private ExecutorThread(AsyncOperationCallback<Double> operationCallback) {
            this.operationCallback = operationCallback;
        }

        @Override
        public void run() {
            int i = 0;
            int j = 0;

            try {
                for (i = 0; i < getMatrixOneLinesCount(); i++)
                    for (j = 0; j < getMatrixTwoColumnsCount(); j++)
                        getExecutorService()
                                .submit(
                                new AsyncMatrixMultiplication(
                                        this.operationCallback, getMatrixOne(),
                                        getMatrixTwo(), i, j
                                )
                        );
            } catch (RejectedExecutionException exception) {
                operationCallback.error(new AsyncOperationException(exception), i, j);
            }
        }
    }
}
