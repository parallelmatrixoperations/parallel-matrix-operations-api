package br.ufu.matrix.task.async.division;

import br.ufu.matrix.task.async.division.exception.AsyncMatrixDivisionException;
import br.ufu.matrix.task.async.multiplication.exception.AsyncMatrixMultiplicationException;
import br.ufu.matrix.task.async.operation.AsyncOperation;
import br.ufu.matrix.task.async.operation.callback.AsyncOperationCallback;
import br.ufu.matrix.task.async.operation.exception.AsyncOperationException;

import static java.util.Objects.isNull;

public class AsyncMatrixDivision extends AsyncOperation<Double[], Double> {

    public AsyncMatrixDivision(AsyncOperationCallback<Double[]> callback,
                               Double[][] matrixOne, Double[][] matrixTwo,
                               Integer lineIndex) {
        super(callback, matrixOne, matrixTwo, lineIndex, null);
    }

    @Override
    public Double[] execute() throws AsyncOperationException {
        try {
            Double[][] matrixOne = getMatrixOne();
            Double[][] matrixTwo = getMatrixTwo();

            if (isNull(matrixOne))
                throw new AsyncMatrixDivisionException("Matrix one is null");

            if (isNull(matrixTwo))
                throw new AsyncMatrixDivisionException("Matrix two is null");

            int lineIndex = getLineIndex();

            if (isNull(lineIndex))
                throw new AsyncMatrixMultiplicationException("Line index is null");

            Double[] matrixOneLine = matrixOne[lineIndex];
            Double[] matrixTwoLine = matrixTwo[lineIndex];

            if (isNull(matrixOneLine))
                throw new AsyncMatrixMultiplicationException("Matrix one null line");

            if (isNull(matrixTwoLine))
                throw new AsyncMatrixMultiplicationException("Matrix two null line");

            int matrixOneColumnsCount = matrixOneLine.length;
            int matrixOneLinesCount = matrixOne.length;
            int matrixTwoColumnsCount = matrixTwoLine.length;
            int matrixTwoLinesCount = matrixTwo.length;

            if ((matrixOneColumnsCount != matrixTwoColumnsCount) || (matrixOneLinesCount != matrixTwoLinesCount))
                throw new AsyncMatrixDivisionException("Invalid division");

            Double[] result = new Double[matrixOneColumnsCount];

            for (int i = 0; i < matrixOneColumnsCount; i++) {
                Double matrixOneColumnValue = matrixOneLine[i];
                Double matrixTwoColumnValue = matrixTwoLine[i];

                if (isNull(matrixOneColumnValue))
                    throw new AsyncMatrixDivisionException("Matrix one null column value");

                if (isNull(matrixTwoColumnValue))
                    throw new AsyncMatrixDivisionException("Matrix two null column value");

                if (matrixTwoColumnValue == 0)
                    result[i] = 0D;
                else
                    result[i] = matrixOneColumnValue / matrixTwoColumnValue;
            }

            return result;
        } catch (AsyncMatrixDivisionException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new AsyncMatrixDivisionException(exception);
        }
    }
}
