package br.ufu.matrix.task.async.executor;

import br.ufu.matrix.task.async.executor.callback.AsyncOperationExecutorCallback;
import br.ufu.matrix.task.async.executor.exception.AsyncOperationExecutorException;
import br.ufu.matrix.task.async.operation.AsyncOperation;

import java.lang.reflect.Array;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Objects.isNull;

public abstract class AsyncOperationExecutor<T, E> {

    public Integer matrixOneLinesCount;
    public Integer matrixTwoLinesCount;
    public Integer matrixOneColumnsCount;
    public Integer matrixTwoColumnsCount;
    private AsyncOperation<T, E> asyncOperation;
    private Integer threads;
    private E[][] matrixOne;
    private E[][] matrixTwo;
    private E[][] result;
    private Class<E> matrixClass;
    private ExecutorService executorService;

    private AtomicInteger counter;

    public AsyncOperationExecutor(Class<E> matrixClass) {
        this.matrixClass = matrixClass;
    }

    public abstract void execute(AsyncOperationExecutorCallback<E> callback) throws AsyncOperationExecutorException;

    public AsyncOperationExecutor<T, E> threads(int thread) {
        this.threads = thread;

        return this;
    }

    public AsyncOperationExecutor<T, E> matrixOne(E[][] matrixOne) {
        this.matrixOne = matrixOne;

        return this;
    }

    public AsyncOperationExecutor<T, E> matrixTwo(E[][] matrixTwo) {
        this.matrixTwo = matrixTwo;

        return this;
    }

    public AsyncOperationExecutor<T, E> executorService(ExecutorService executorService) {
        this.executorService = executorService;

        return this;
    }

    public abstract int getCounterTotal();

    @SuppressWarnings("unchecked")
    public AsyncOperationExecutor<T, E> build() throws AsyncOperationExecutorException {
        if (isNull(this.threads))
            this.threads = Runtime.getRuntime().availableProcessors();

        if (isNull(this.executorService))
            this.executorService = Executors.newFixedThreadPool(threads);

        if (isNull(matrixOne))
            throw new AsyncOperationExecutorException("Matrix one is null");

        if (isNull(matrixTwo))
            throw new AsyncOperationExecutorException("Matrix two is null");

        this.matrixOneLinesCount = matrixOne.length;
        this.matrixTwoLinesCount = matrixTwo.length;

        E[] firstMatrixOneLine = matrixOne[0];
        E[] firstMatrixTwoLine = matrixTwo[0];

        if (isNull(firstMatrixOneLine))
            throw new AsyncOperationExecutorException("Matrix one null line");

        if (isNull(firstMatrixTwoLine))
            throw new AsyncOperationExecutorException("Matrix two null line");

        this.matrixOneColumnsCount = firstMatrixOneLine.length;
        this.matrixTwoColumnsCount = firstMatrixTwoLine.length;

        this.result = (E[][]) Array.newInstance(matrixClass, matrixOneLinesCount, matrixTwoColumnsCount);

        this.counter = new AtomicInteger(getCounterTotal());

        return this;
    }

    public AsyncOperationExecutor<T, E> asyncOperation(AsyncOperation<T, E> asyncOperation) {
        this.asyncOperation = asyncOperation;

        return this;
    }

    public AsyncOperation<T, E> getAsyncOperation() {
        return asyncOperation;
    }

    public Integer getThreads() {
        return threads;
    }

    public E[][] getMatrixOne() {
        return matrixOne;
    }

    public E[][] getMatrixTwo() {
        return matrixTwo;
    }

    public E[][] getResult() {
        return result;
    }

    public Integer getMatrixOneLinesCount() {
        return matrixOneLinesCount;
    }

    public Integer getMatrixTwoLinesCount() {
        return matrixTwoLinesCount;
    }

    public Integer getMatrixOneColumnsCount() {
        return matrixOneColumnsCount;
    }

    public Integer getMatrixTwoColumnsCount() {
        return matrixTwoColumnsCount;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public AtomicInteger getCounter() {
        return counter;
    }
}
