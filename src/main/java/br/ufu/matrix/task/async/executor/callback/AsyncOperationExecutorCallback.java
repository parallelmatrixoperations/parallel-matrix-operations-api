package br.ufu.matrix.task.async.executor.callback;

import br.ufu.matrix.task.async.executor.exception.AsyncOperationExecutorException;

public interface AsyncOperationExecutorCallback<T> {

    void success(T[][] result);

    void error(AsyncOperationExecutorException error);
}
